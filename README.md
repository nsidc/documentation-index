# PROJECT STATUS

**The documentation-index stack appears to be the only user of the config-injector service. If this stack is retired, the config-injector should be retired as well.**

# README #

### What is this repository for? ###

* This is a Sinatra service used to expose api documentation for our services.  documentation-index-ui will consume the response via JavaScript to create a front end for viewing and interacting with our services, including the ability to issue test api calls.

## Developer info

### Dependencies

* Configuration service : https://bitbucket.org/nsidc/config-injector-service/overview
    * Service that acts as a lookup for the database password
    * Since this is only used for configuration info, you could use your own system for connecting to the db.

* Database
    * The database for this project stores information about services that publicly expose swagger JSON documentation objects

### Building

```bash
git clone git@bitbucket.org:nsidc/documentation-index.git
cd documentation-index
bundle install
```

### Running

```bash
bundle exec rake run
```

### Database migrations

Make sure you specify a RACK_ENV when migrating the db.
Sometimes doing a migration on the test db is necessary before tests will succeed.

```bash
bundle exec rake db:migrate RACK_ENV=${test/integration/qa...}
```

### Testing

If tests fail because of missing tables, first migrate the db with RACK_ENV=test.

```bash
bundle exec rake spec:unit
```

### How to contact NSIDC ###

User Services and general information:  
Support: http://support.nsidc.org  
Email: nsidc@nsidc.org  

Phone: +1 303.492.6199  
Fax: +1 303.492.2468  

Mailing address:  
National Snow and Ice Data Center  
CIRES, 449 UCB  
University of Colorado  
Boulder, CO 80309-0449 USA  

### License ###

Every file in this repository is covered by the GNU GPL Version 3; a copy of the
license is included in the file COPYING.