require 'sinatra/activerecord'

class Service < ActiveRecord::Base

  def self.create(params)
    params = permit params
    super params
  end

  def update_attributes(params)
    super(self.class.permit(params))
  end

  def to_swagger_object
    {
      path: "#{ endpoint }#{ documentation_endpoint }",
      description: "#{ name }"
    }
  end

  def self.valid_attributes
    %w(
      name
      endpoint
      documentation_endpoint
      is_advertised
    )
  end

  def self.permit(params)
    params.keep_if { |p| valid_attributes.include? p }
  end

end
