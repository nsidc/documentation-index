require 'sinatra/activerecord'

class DatabaseConnection
  class << self

    def connect(env)
      db_config = YAML.load_file(File.join(File.dirname(__FILE__), '..', 'config', 'database.yml'))

      if env == 'test'
        # Loading from YAML specifies a path that is relative to the dir you are running rake spec:unit
        # from within.
        db_config[env]['database'] = File.join(File.dirname(__FILE__), '..', 'db', 'testdb.sqlite3')
      else
        config_client = Nsidc::ConfigInjectorClient.new(
          'http://config-injector-vm.apps.int.nsidc.org:10680',
          env
        )

        db_config[env]['password'] = config_client.get('documentation-index.password')
      end

      ActiveRecord::Base.establish_connection db_config[env]
    end

  end
end
