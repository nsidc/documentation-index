require 'ipaddr'

class Authentication
  class << self

    # use CIDR addrs to see if client ip is included within the range.
    def authenticated?(client_ip)
      authenticated = false

      valid_ips.each { |valid_ip|
        authenticated ||= valid_ip.include? client_ip
      }

      authenticated
    end

    private

    # https://nsidc.org/confluence/display/TECHS/List+of+Networks
    # allow nsidc private networks, nsidc custom vpn.
    # 10/8, 0/8 and 192.168/16 are reserved.
    def valid_ips
      [
        IPAddr.new('172.18.224.0/19'),
        IPAddr.new('172.21.34.192/26'),
        IPAddr.new('10.0.0.0/8'),
        IPAddr.new('0.0.0.0/8'),
        IPAddr.new('127.0.0.0/8'),
        IPAddr.new('192.168.0.0/16')
      ]
    end

  end
end
