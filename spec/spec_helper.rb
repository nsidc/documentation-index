# http://recipes.sinatrarb.com/p/testing/rspec

ENV['RACK_ENV'] = 'test'

require File.join(File.dirname(__FILE__), '..', 'app')
require 'rack/test'

module RSpecMixin
  include Rack::Test::Methods
  def app
    Sinatra::Application
  end
end

RSpec.configure { |c| c.include RSpecMixin }
