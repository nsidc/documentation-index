require File.join(File.dirname(__FILE__), '.', 'spec_helper')

describe 'root' do
  it 'should give http 200' do
    get '/'
    last_response.should be_ok
  end
end

describe 'api_docs route' do
  it 'should give http 200' do
    get '/api-docs'
    last_response.should be_ok
  end
end

describe 'post a new service route' do

  # clean up
  after(:each) do
    Service.where(name: service_hash[:name]).destroy_all
  end

  it 'should give http 200' do
    post '/protected/service', service_hash
    last_response.should be_ok
  end

  it 'should create a new service' do
    post '/protected/service', service_hash
    Service.where(name: service_hash[:name]).size.should be > 0
  end

  def service_hash
    {
      name: 'test_svc',
      endpoint: 'http://foo.com/test_endpoint',
      documentation_endpoint: '/foo_docs',
      is_advertised: true
    }
  end
end
