require File.join(File.dirname(__FILE__), '.', 'spec_helper')
require File.join(File.dirname(__FILE__), '..', 'models', 'authentication')

describe 'authentication' do

  # 172.18.224.0/19
  it 'should allow access to NSIDC private networks' do
    Authentication.authenticated?('172.18.224.0').should be true
    Authentication.authenticated?('172.18.224.127').should be true
    Authentication.authenticated?('172.18.224.255').should be true
    Authentication.authenticated?('172.18.255.127').should be true
    Authentication.authenticated?('172.18.255.255').should be true
  end

  # 172.21.34.192/26
  it 'should allow access to NSIDC custom vpn' do
    Authentication.authenticated?('172.21.34.192').should be true
    Authentication.authenticated?('172.21.34.198').should be true
    Authentication.authenticated?('172.21.34.224').should be true
    Authentication.authenticated?('172.21.34.255').should be true
  end

  it 'should allow access from reserved addresses and localhost' do
    Authentication.authenticated?('0.1.2.3').should be true
    Authentication.authenticated?('0.0.2.2').should be true

    Authentication.authenticated?('10.0.100.255').should be true
    Authentication.authenticated?('10.0.0.0').should be true

    Authentication.authenticated?('127.0.0.0').should be true
    Authentication.authenticated?('127.255.255.255').should be true

    Authentication.authenticated?('192.168.1.1').should be true
    Authentication.authenticated?('192.168.255.255').should be true
  end

  it 'should deny access to the open internet' do
    Authentication.authenticated?('17.1.2.3').should be false
    Authentication.authenticated?('173.1.1.2').should be false
    Authentication.authenticated?('201.21.1.2').should be false
    Authentication.authenticated?('236.21.1.0').should be false
    Authentication.authenticated?('242.0.0.0').should be false
  end

end
