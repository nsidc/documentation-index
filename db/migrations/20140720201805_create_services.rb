class CreateServices < ActiveRecord::Migration
  def change
    create_table :services do |t|
      t.string :name
      t.string :endpoint
      t.string :documentation_endpoint
    end
  end
end
