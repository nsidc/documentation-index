class AddDatasetCatalogServices < ActiveRecord::Migration
 def up
    if ENV['RACK_ENV'].nil?
      raise ActiveRecord::Rollback, "Cannot insert Dataset Catalog Services without environment defined"
    end
    if ENV['RACK_ENV'] != 'production'
       url_string = "http://#{ENV['RACK_ENV']}.nsidc.org/api/dataset/metadata"
    else
       url_string = 'http://nsidc.org/api/dataset/metadata'
    end
    execute "INSERT INTO services (name,endpoint,documentation_endpoint) values ('Dataset Catalog Services','#{url_string}','/SwaggerDocs')"
 end
 def down
    execute "DELETE FROM services where name = 'Dataset Catalog Services'"
 end
end
