class AddIsAdvertisedToServices < ActiveRecord::Migration
  def change
    add_column :services, :is_advertised, :boolean, { :null => false, :default => true }
  end
end
