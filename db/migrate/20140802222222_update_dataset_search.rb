class UpdateDatasetSearch < ActiveRecord::Migration
  def change
    reversible do |insert|
      insert.up do
        if ENV['RACK_ENV'].nil? 
          raise ActiveRecord::Rollback, "Cannot update Dataset search endpoint without environment defined"
        end
        if ENV['RACK_ENV'] != 'production'
          url_string = "http://#{ENV['RACK_ENV']}.nsidc.org/api/dataset/2"
        else
          url_string = 'http://nsidc.org/api/dataset/2'
        end
        execute "UPDATE services set endpoint='#{url_string}' where name='Dataset search'"
      end

      insert.down do
        raise ActiveRecord::IrreversibleMigration, "Can't recover updated value for Dataset search"
      end
    end
  end
end
