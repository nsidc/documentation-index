class AddDatasetSearch < ActiveRecord::Migration
  def change
    reversible do |insert|
      insert.up do
        execute <<-SQL
          INSERT INTO services (name,endpoint,documentation_endpoint) values ('Dataset search','http://integration.nsidc.org/api/dataset/2','/SwaggerDocs')
        SQL
      end
      insert.down do
        execute <<-SQL
          DELETE FROM services where name='Dataset search'
        SQL
      end
    end
  end
end
