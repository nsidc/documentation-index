require 'sinatra'
require 'pg'
require 'yaml'
require 'json'
require 'nsidc/config_injector_client'
require_relative 'models/service'
require_relative 'models/authentication'
require_relative 'models/database_connection'

use Rack::Logger

current_env = ENV['RACK_ENV'] || 'development'
DatabaseConnection.connect(current_env)

before do
  content_type :json
  headers 'Access-Control-Allow-Origin' => '*',
    'Access-Control-Allow-Methods' => %w(OPTIONS GET POST PUT DELETE),
    'Access-Control-Allow-Headers' => 'Content-Type'
end

# Make sure the visitor is coming from a trusted source
before '/protected/*' do
  halt unless Authentication.authenticated? request.ip
end

options '/*' do
end

get '/' do
  Service.all.to_json
end

get '/api-docs' do
  response = JSON.parse(File.read('swagger-config-pretty.json'))

  visible_services = Authentication.authenticated?(request.ip) ? Service.all : Service.where(is_advertised: true)
  visible_services.each { |svc|
    response["apis"] << svc.to_swagger_object
  }

  response.to_json
end

post '/protected/service' do
  service = Service.create(read_cors_params(params))
  service.to_json
end

put '/protected/service/:id' do
  service = Service.find(params[:id]).update_attributes(read_cors_params(params))
  service.to_json
end

delete '/protected/service/:id' do
  Service.find(params[:id]).destroy
end

private

# Some libs (angular) send the CORS params in the rack.input,
# For testing and other consumers we can just use the straight params hash.
def read_cors_params(params)
  return params.merge JSON.parse(request.env['rack.input'].read)
  rescue
    return params
end
