require 'bundler'
require File.join(File.dirname(__FILE__), 'app')
require File.join(File.dirname(__FILE__), 'config', 'app_config')

Bundler.require :default

run Sinatra::Application
